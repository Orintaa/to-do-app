const todoButton = document.getElementById("todoBtn");
const todoInput = document.getElementById("todoInput");
const todoList = document.getElementById("todoList");

todoButton.addEventListener("click", () => {
    const inputValue = todoInput.value;
    creatListItem(inputValue);
  });

  const creatListItem = (text) => {
    const listItem = document.createElement("li");
    const listName = document.createElement("span");
    const spanText = document.createTextNode(text);
    const listButton = document.createElement("button");
    const buttonText = document.createTextNode("x");
    listItem.appendChild(listName);
    listItem.appendChild(listButton);
    listButton.appendChild(buttonText);
    listName.appendChild(spanText);
    todoList.appendChild(listItem);

    listButton.addEventListener("click", (e) => {
        e.target.parentNode.remove();
    })

    listName.addEventListener("click", (e) => {
        e.target.classList.toggle("marked");
    })
  }

 



